import("stdfaust.lib");

smoothSelect2(bs, s) = ba.selectmulti(bs, ((_,!), (!,_)), s);


// Calculate a denormalized frequency in hz from offset to max_fq from a (normalized) 0.0-1.0 float value
denormalize_fq(fq, offset, max_fq) = (fq * fq)  : * (max_fq - offset) + offset;

// Calculate a denormalized frequency in hz from offset to max_fq from a (normalized) 0.0-1.0 float value
denormalize_res(res, offset, max_res) = (res)  : * (max_res - offset) + offset;

subFilter(on, hp, bp, lp, freq, res) = tick ~ (_,_) <: !,!,mix
with {
    	tick(ic1eq, ic2eq, v0) =
			2*v1 - ic1eq,
			2*v2 - ic2eq,
			v0, v1, v2
		with {
			v1 = ic1eq + g *(v0-ic2eq) : /(1 + g*(g+k));
			v2 = ic2eq + g * v1;
		};

		F = denormalize_fq(freq, 70, 7000);
		Q = denormalize_res(res, 0.5, 3);

		A = pow(10.0, 0.0/40.0);

		g = tan(F * ma.PI/ma.SR);

		k = 1/Q;

		mix = 
            _,_,_
                <:
                    _,!,!,
                    (_,_,_
                        <:  
                            (si.dot(3, 1,-k,-1) : smoothSelect2(ma.BS * 5, hp, 0)),
                            (si.dot(3, 0, 1, 0) : smoothSelect2(ma.BS * 5, bp, 0)),
                            (si.dot(3, 0, 0, 1) : smoothSelect2(ma.BS * 5, lp, 0))
                        :> _
                    ) : smoothSelect2(ma.BS * 5, on * (hp+bp+lp > 0));
};


on = checkbox("on");
hp = checkbox("hp");
lp = checkbox("lp");
bp = checkbox("bp");

freq = hslider("freq", 0, 0, 1, 0.001);
resonance = hslider("resonance", 0, 0, 1, 0.001);


process = _,_ : par(i, 2, subFilter(on, hp, bp, lp, freq, resonance));
