# Create daisy patch

```
faust2daisy -source ./subfilter.dsp && mv subfilter subfilter-daisy
```

# Compile daisy patch

1. First make sure that you have following checkouts next to the subfilter folder (otherwise you need to change the environment variables `LIBDAISY_DIR` & `DAISYSP_DIR`).

- [DaisySP](https://github.com/electro-smith/DaisySP.git)
-  [libDaisy](https://github.com/electro-smith/libDaisy.git)

2. Make sure you have following dependencies installed:
- `arm-none-eabi-gcc`
- `arm-none-eabi-newlib`

3. Then make sure both `DaisySP` and `libDaisy` are built. Simply run `make` in the respective checkout.

4. Then run following command in the `subfilter-daisy` folder:
```
LIBDAISY_DIR=../../libDaisy/ DAISYSP_DIR=../../DaisySP/ make
```

# Flashing daisy

## DFU

1. make sure you have `dfu-util` installed
2. Connect daisy in DFU mode??
3. Run `LIBDAISY_DIR=../../libDaisy/ DAISYSP_DIR=../../DaisySP/ make program-dfu` in the `subfilter-daisy` subfolder

## OpenOCD

1. make sure you have `openocd` installed
2. Connect daisy in DFU mode??
3. Run `LIBDAISY_DIR=../../libDaisy/ DAISYSP_DIR=../../DaisySP/ make program` in the `subfilter-daisy` subfolder


